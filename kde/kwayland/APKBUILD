# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwayland
pkgver=5.55.0
pkgrel=0
pkgdesc="Qt-style Client and Server library wrapper for the Wayland libraries"
arch="all"
url="https://www.kde.org"
license="LGPL-2.1"
depends_dev="qt5-qtbase-dev wayland-dev"
makedepends="$depends_dev extra-cmake-modules doxygen qt5-qttools-dev"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Fails due to requiring running Wayland compositor

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="$pkgdir" install
}

sha512sums="ab76f74784bcda078eaac6c6efaeb0f6158439bc1d51eca3f70f721b2acd8bc5c12bd2a81e4b289c881b95fd186336517903adf30803e76cf4795eaf38761b05  kwayland-5.55.0.tar.xz"
