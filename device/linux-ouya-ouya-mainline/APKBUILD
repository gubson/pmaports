# Kernel config based on: arch/arm/configs/tegra_defconfig

pkgname="linux-ouya-ouya-mainline"
pkgver=5.0
pkgrel=0
pkgdesc="Ouya kernel"
arch="armv7"
_carch="arm"
_flavor="ouya-ouya-mainline"
url="https://kernel.org"
license="GPL2"
options="!strip !check !tracedeps"
makedepends="perl sed installkernel bash gmp-dev bc linux-headers elfutils-dev xz flex bison openssl-dev"

# Compiler: latest GCC from Alpine
HOSTCC="${CC:-gcc}"
HOSTCC="${HOSTCC#${CROSS_COMPILE}}"

# Source
_repository="linux"
_rev="30a3374f0d45dfd1a07861afb0f29ca117f12a31"
_config="config-${_flavor}.${arch}"
source="
	$pkgname-$_rev.tar.gz::https://github.com/decatf/${_repository}/archive/${_rev}.tar.gz
	$_config
"
builddir="$srcdir/${_repository}-${_rev}"

prepare() {
	default_prepare

	mkdir -p "$srcdir"/build
	cp -v "$srcdir"/$_config "$srcdir"/build/.config
	make -C "$builddir" O="$srcdir"/build ARCH="$_carch" HOSTCC="$HOSTCC" \
		olddefconfig
}

build() {
	cd "$srcdir"/build
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-${_flavor}" \
		CFLAGS_MODULE=-fno-pic
}

package() {
	cd "$srcdir/build/arch/${_carch}/boot"

	if [ "$CARCH" == "aarch64" ]; then
		install -Dm644 "$srcdir/build/arch/${_carch}/boot/Image" \
			"$pkgdir/boot/vmlinuz-$_flavor"
	else
		install -Dm644 "$srcdir/build/arch/${_carch}/boot/"*zImage \
			"$pkgdir/boot/vmlinuz-$_flavor"
	fi

	install -D "$srcdir/build/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	cd "$srcdir"/build

	local _install
	case "$CARCH" in
	aarch64*|arm*)	_install="modules_install dtbs_install" ;;
	*)		_install="modules_install" ;;
	esac

	make -j1 $_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}


sha512sums="3fddef074abb275c24db91db24d511f7d72346e5a39b1ad2d3b1a66bab18afcd336a8ae402b2c4fbad78fcde700b0f2adaabc41cd25a9a4fd6003bdc0de19990  linux-ouya-ouya-mainline-30a3374f0d45dfd1a07861afb0f29ca117f12a31.tar.gz
5bf62b3aa63356935988d02729a51e358deb0e885aec032fa34f5f323cff080b18a21b00aaeac351f2d260c42a41d80498607d39d7b6270ab00798195720ef31  config-ouya-ouya-mainline.armv7"
